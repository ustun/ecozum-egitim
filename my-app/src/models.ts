import * as uuid from "uuid/v4";

export enum FilterT {
  All = "All",
  Active = "Active",
  Completed = "Completed"
}

export class TodoModel {
  completed = false;
  id: string = uuid();

  constructor(public description: string) {}
}

class TodoAppModel {
  todos: Array<TodoModel> = [];
  tempTodo = "";
  filter = FilterT.All;
  searchValue = "";

  insertTodo(description: string) {
    this.todos.push(new TodoModel(description));
  }

  insertTodoFromTempTodo() {
    this.insertTodo(this.tempTodo);
    this.tempTodo = "";
  }

  setTempTodo(x: string) {
    this.tempTodo = x;
  }

  toggleTodo(id: string) {
    this.todos.forEach(function(todo) {
      if (todo.id == id) {
        todo.completed = !todo.completed;
      }
    });
  }

  setSearchValue(x: string) {
    this.searchValue = x;
  }

  getNTodos() {
    return this.todos.filter(todo => !todo.completed).length;
  }

  getFilteredTodos(): Array<TodoModel> {
    switch (this.filter) {
      case FilterT.All:
        return this.todos;
      case FilterT.Completed:
        return this.todos.filter(x => x.completed);
      case FilterT.Active:
        return this.todos.filter(x => !x.completed);
    }
  }

  getVisibleTodos(): Array<TodoModel> {
    return this.getFilteredTodos().filter(todo =>
      todo.description.toLowerCase().includes(this.searchValue.toLowerCase())
    );
  }

  setFilter(f: FilterT) {
    this.filter = f;
  }

  setTodos(todos: Array<TodoModel>) {
    this.todos = todos;
  }

  async fetchTodos() {
    const data: { results: Array<{ title: string }> } = await fetch(
      "https://swapi.co/api/films/"
    ).then(x => x.json());
    this.setTodos(data.results.map(film => new TodoModel(film.title)));
    return data;
  }
}

export default TodoAppModel;
