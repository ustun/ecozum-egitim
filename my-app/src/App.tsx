import * as React from "react";

import TodoAppModel, { FilterT, TodoModel } from "./models";

class TodoItemComponent extends React.Component<
  { item: TodoModel; onToggle: () => void },
  object
> {
  render() {
    const { item } = this.props;

    return (
      <li>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={item.completed}
            onChange={this.props.onToggle}
          />
          <label>{item.description}</label>
          {/* <div>{this.props.item.id}</div> */}
          <button className="destroy" />
        </div>
        <input className="edit" defaultValue={item.description} />
      </li>
    );
  }
}

function FilterComponent(props: {
  filter: FilterT;
  selected: boolean;
  onClick: (x: FilterT) => void;
}) {
  return (
    <li>
      <a
        onClick={() => props.onClick(props.filter)}
        href="#/active"
        className={props.selected ? "selected" : ""}
      >
        {props.filter}
      </a>
    </li>
  );
}

const model = new TodoAppModel();

class App extends React.Component<object, { model: TodoAppModel }> {
  constructor(props: object) {
    super(props);
    this.state = { model };
  }

  async componentDidMount() {
    await model.fetchTodos();
    this.setState({ model });
  }

  setFilter = (newFilter: FilterT) => {
    model.setFilter(newFilter);
    this.setState({ model });
  };

  renderHeader() {
    return (
      <header id="header">
        <h1>todos</h1>
        <input
          id="new-todo"
          value={model.tempTodo}
          onChange={e => {
            model.setTempTodo(e.target.value);
            this.setState({ model });
          }}
          placeholder="What needs to be done?"
          onKeyUp={e => {
            if (e.key == "Enter") {
              model.insertTodoFromTempTodo();
              this.setState({ model });
            }
          }}
          autoFocus
        />
        <input
          id="new-todo"
          value={model.searchValue}
          onChange={e => {
            model.setSearchValue(e.target.value);
            this.setState({ model });
          }}
          placeholder="Search"
        />
      </header>
    );
  }

  renderFooter() {
    return (
      <footer id="footer" style={{ display: "block" }}>
        <span id="todo-count">
          <strong>{model.getNTodos()}</strong> items left
        </span>
        <ul id="filters">
          <FilterComponent
            filter={FilterT.All}
            selected={model.filter == "All"}
            onClick={this.setFilter}
          />

          <FilterComponent
            filter={FilterT.Completed}
            selected={model.filter == "Completed"}
            onClick={this.setFilter}
          />
          <FilterComponent
            filter={FilterT.Active}
            selected={model.filter == "Active"}
            onClick={this.setFilter}
          />
        </ul>
      </footer>
    );
  }

  renderMain() {
    return (
      <section id="main" style={{ display: "block" }}>
        <input id="toggle-all" type="checkbox" />
        <label htmlFor="toggle-all">Mark all as complete</label>
        <ul id="todo-list">
          {model.getVisibleTodos().map(item => (
            <TodoItemComponent
              item={item}
              key={item.id}
              onToggle={() => {
                model.toggleTodo(item.id);
                this.setState({ model });
              }}
            />
          ))}
        </ul>
      </section>
    );
  }

  render() {
    return (
      <section id="todoapp">
        {this.renderHeader()}
        {this.renderMain()}
        {this.renderFooter()}
      </section>
    );
  }
}

export default App;
