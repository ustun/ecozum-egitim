import { createStore } from "redux";
import uuid from "uuid/v4";

const initialState = { todoList: [], geciciTodo: "", filter: "All" };

function insertTodo(state) {
  return {
    ...state,
    geciciTodo: "",
    todoList: [
      ...state.todoList,
      {
        description: state.geciciTodo,
        id: uuid(),
        completed: false
      }
    ]
  };
}

function toggleTodo(state, action) {
  return {
    ...state,
    todoList: state.todoList.map(todo => {
      if (todo.id === action.id) {
        return { ...todo, completed: !todo.completed };
      } else {
        return todo;
      }
    })
  };
}

const actionTypes = {
  SET_FILTER: "SET_FILTER",
  SET_GECICI_TODO: "SET_GECICI_TODO"
};
function TodoAppReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_GECICI_TODO:
      return { ...state, geciciTodo: action.payload };
    case "INSERT_GECICI_TODO":
      return insertTodo(state);
    case "SET_FILTER":
      return {
        ...state,
        filter: action.payload
      };
    case "TOGGLE_TODO":
      return toggleTodo(state, action);
  }

  return state;
}

const model = createStore(TodoAppReducer);

const selectors = {
  getVisibleTodos(state) {
    switch (state.filter) {
      case "All":
        return state.todoList;
      case "Active":
        return state.todoList.filter(x => !x.completed);
      case "Completed":
        return state.todoList.filter(x => x.completed);
    }
  },

  nLeftTodos(state) {
    return state.todoList.filter(x => !x.completed).length;
  }
};

const actionCreators = {
  setFilter(text) {
    return { type: actionTypes.SET_FILTER, payload: text };
  }
};

export { model, selectors, actionCreators };

// todoStore.subscribe(function() {
//   console.log(
//     "todo store degeri",
//     JSON.stringify(todoStore.getState(), null, 4)
//   );
// });

// // var ajan1 = function(action) {
// //   return console.log("action geldi", JSON.stringify(action, null, 4));
// // };
// // todoStore.addMiddleware(ajan1);

// todoStore.dispatch({ type: "SET_GECICI_TODO", text: "Peynir al" });

// todoStore.dispatch({ type: "INSERT_GECICI_TODO" });

// todoStore.dispatch({ type: "SET_GECICI_TODO", text: "Domates al" });

// todoStore.dispatch({ type: "INSERT_GECICI_TODO" });
