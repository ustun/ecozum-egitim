import React from "react";
import { model, selectors, actionCreators } from "./Todo";

class TodoItem extends React.Component {
  render() {
    const { item } = this.props;
    return (
      <li>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            onChange={e => model.dispatch({ type: "TOGGLE_TODO", id: item.id })}
          />
          <label>{item.description}</label>
          <button className="destroy" />
        </div>
        <input className="edit" defaultValue={item.description} />
      </li>
    );
  }
}

export default class TodoApp extends React.Component {
  constructor() {
    super();
    this.state = model.getState();
    debugger;
    model.subscribe(() => {
      this.setState(model.getState());
    });
  }
  render() {
    return (
      <section id="todoapp">
        <header id="header">
          <h1>todos</h1>
          <input
            id="new-todo"
            placeholder="What needs to be done?"
            onKeyPress={e => {
              if (e.key == "Enter") {
                model.dispatch({ type: "INSERT_GECICI_TODO" });
              }
            }}
            onChange={e =>
              model.dispatch({
                type: "SET_GECICI_TODO",
                payload: e.target.value
              })}
            autofocus
          />
        </header>
        <section id="main" style={{ display: "block" }}>
          <input id="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>
          <ul id="todo-list">
            {selectors
              .getVisibleTodos(this.state)
              .map(item => <TodoItem key={item.id} item={item} />)}
          </ul>
        </section>
        <footer id="footer" style={{ display: "block" }}>
          <span id="todo-count">
            <strong>{selectors.nLeftTodos(this.state)}</strong> items left
          </span>
          <ul id="filters">
            <li>
              <a
                onClick={e => model.dispatch(actionCreators.setFilter("All"))}
                href="#/"
              >
                All
              </a>
            </li>
            <li>
              <a
                onClick={e =>
                  model.dispatch(actionCreators.setFilter("Active"))}
                href="#/active"
              >
                Active
              </a>
            </li>
            <li>
              <a
                onClick={e =>
                  model.dispatch(actionCreators.setFilter("Completed"))}
                href="#/completed"
              >
                Completed
              </a>
            </li>
          </ul>
        </footer>
      </section>
    );
  }
}
