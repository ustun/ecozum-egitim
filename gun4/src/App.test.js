import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as model from "./model";
import * as selectors from "./selectors";
import { createMyStore, TodosReducer } from "./model";

const { actionTypes } = model;

describe("TodosReducer", () => {
  test("todoreducer", () => {
    expect(TodosReducer(undefined, { type: "@@REDUX/INIT" }).length).toBe(0);

    TodosReducer([{ id: 1, completed: false, description: "hello" }], {
      type: actionTypes.TODO_CEVIR,
      payload: 1
    });

    expect(
      TodosReducer([{ id: 1, completed: false, description: "hello" }], {
        type: actionTypes.TODO_CEVIR,
        payload: 1
      })
    ).toEqual([{ id: 1, completed: true, description: "hello" }]);
  });
});

describe("TodoApp", () => {
  let store;

  beforeEach(() => {
    store = model.createMyStore();
  });

  test("gecici todo eklenebilmeli", () => {
    expect(store.getState().geciciTodo).toEqual("");
    store.dispatch({
      type: model.actionTypes.SET_GECICI_TODO,
      payload: "merhaba"
    });
    expect(store.getState().geciciTodo).not.toBe("");
    expect(store.getState().geciciTodo).toBe("merhaba");
  });

  test("gecici todo eklendikten sonra ana listeye eklenmeli", () => {
    expect(store.getState().todos.length).toBe(0);
    store.dispatch({
      type: model.actionTypes.SET_GECICI_TODO,
      payload: "merhaba"
    });
    store.dispatch({ type: actionTypes.TODO_EKLE });
    expect(store.getState().todos.length).toBe(1);
    expect(store.getState().geciciTodo).toBe("");
  });

  test("todolar tamamlanmis olarak isaretlenebilmeli", () => {
    store.dispatch({
      type: model.actionTypes.SET_GECICI_TODO,
      payload: "merhaba"
    });

    expect(selectors.getNTodos(store.getState())).toBe(0);
    store.dispatch({ type: actionTypes.TODO_EKLE });
    var id = store.getState().todos[0].id;
    expect(selectors.getNCompletedTodos(store.getState())).toBe(0);
    expect(selectors.getNTodos(store.getState())).toBe(1);

    store.dispatch({ type: actionTypes.TODO_CEVIR, payload: id });

    expect(selectors.getNCompletedTodos(store.getState())).toBe(1);
    expect(selectors.getNTodos(store.getState())).toBe(1);
  });

  test("todo silmeyi test et", () => {
    let todos;

    todos = [
      { completed: false, id: 1, description: "hello" },
      { completed: true, id: 2, description: "hello" }
    ];

    store = createMyStore({
      todos: todos,
      aktifFiltre: "All",
      geciciTodo: ""
    });

    expect(selectors.getNTodos(store.getState())).toBe(2);

    store.dispatch({ type: actionTypes.TODO_SIL, payload: 2 });

    expect(selectors.getNTodos(store.getState())).toBe(1);
  });

  describe("aktif filtre secilsin ve getvisibletodos ona gore degissin", () => {
    let todos;

    beforeEach(() => {
      todos = [
        { completed: false, id: 1, description: "hello" },
        { completed: true, id: 2, description: "hello" }
      ];
    });

    test("aktif filtre = all", () => {
      store = createMyStore({
        todos: todos,
        aktifFiltre: "All",
        geciciTodo: ""
      });

      expect(selectors.getVisibleTodos(store.getState()).length).toBe(2);
    });
    test("aktif filtre = aktif", () => {
      store = createMyStore({
        todos: todos,
        aktifFiltre: "Aktif",
        geciciTodo: ""
      });

      expect(selectors.getVisibleTodos(store.getState()).length).toBe(1);
    });
    test("aktif filtre = tamam", () => {
      store = createMyStore({
        todos: todos,
        aktifFiltre: "Tamamlanmis",
        geciciTodo: ""
      });

      expect(selectors.getVisibleTodos(store.getState()).length).toBe(1);
    });

    test("aktif filtre = sacmalik", () => {
      store = createMyStore({
        todos: todos,
        aktifFiltre: "Sacmalik",
        geciciTodo: ""
      });

      var x = () => selectors.getVisibleTodos(store.getState()).length;
      expect(x).toThrowError(/gecersiz/);
      expect(x).toThrowError(selectors.FooError);
      expect(x).not.toThrowError(selectors.BarError);

      expect(x).not.toThrowError(/merhaba/);
    });
    // store.dispatch({
    //   type: model.actionTypes.SET_GECICI_TODO,
    //   payload: "merhaba"
    // });

    // store.dispatch({ type: actionTypes.TODO_EKLE });

    // store.dispatch({
    //   type: model.actionTypes.SET_GECICI_TODO,
    //   payload: "merhaba"
    // });

    // store.dispatch({ type: actionTypes.TODO_EKLE });
  });
});
