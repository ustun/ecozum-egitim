import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Provider, connect } from "react-redux";
import { TodoAppReducer } from "./model";
import { compose, applyMiddleware, createStore } from "redux";
import TodoAppView from "./TodoAppView";
import { createDevTools } from "redux-devtools";
import { BrowserRouter, Route, Link } from "react-router-dom";

// Monitors are separate packages, and you can make a custom one
import LogMonitor from "redux-devtools-log-monitor";
import DockMonitor from "redux-devtools-dock-monitor";

const logger = store => next => action => {
  if (action.type.includes("GECICI")) {
    next(action);
    return;
  }

  console.group(action.type);

  console.info("dispatching", action);

  console.log("%c onceki state", "color: red;", store.getState());

  let result = next(action);

  console.log("%c next state", "color: green", store.getState());
  console.groupEnd(action.type);
  return result;
};

// Test icin yazildi

// createDevTools takes a monitor and produces a DevTools component
const DevTools = createDevTools(
  // Monitors are individually adjustable with props.
  // Consult their repositories to learn about those props.
  // Here, we put LogMonitor inside a DockMonitor.
  // Note: DockMonitor is visible by default.
  <DockMonitor
    toggleVisibilityKey="ctrl-h"
    changePositionKey="ctrl-q"
    defaultIsVisible={true}
  >
    <LogMonitor theme="tomorrow" />
  </DockMonitor>
);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const createMyStore = defaultState => {
  const enhancer = composeEnhancers(
    // Middleware you want to use in development:
    applyMiddleware(logger)
    // Required! Enable Redux DevTools with the monitors you chose
    // DevTools.instrument()
  );

  const store = createStore(TodoAppReducer, defaultState, enhancer);
  return store;
};

const store = createMyStore();
class App extends Component {
  render() {
    return <div>{this.props.geciciTodo}</div>;
  }
}

function selectFn(db) {
  return { geciciTodo: db.geciciTodo };
}

const AkilliApp2 = connect(selectFn)(App);
// Birbirine denk AkilliApp1 yerine AkilliApp2

class AkilliApp1 extends Component {
  constructor() {
    super();
    this.state = selectFn(store.getState());
    store.subscribe(() => {
      this.setState(selectFn(store.getState()));
    });
  }
  render() {
    return <App geciciTodo={this.state.geciciTodo} />;
  }
}

function UserComponent(props) {
  return (
    <div>
      {JSON.stringify(props, null, 4)}
      This is user {props.match.params.userId}
      <Link to="/">Home</Link>
    </div>
  );
}

class Main extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider store={store}>
          {/* <AkilliApp2 /> */}
          <div>
            <Route
              path="/"
              exact
              render={() => (
                <div>
                  <Link to="/foo">go to foo</Link>

                  {[1, 2, 3].map(userId => (
                    <Link key={userId} to={"/user/" + userId}>
                      go to user {userId}
                    </Link>
                  ))}

                  <TodoAppView />
                </div>
              )}
            />

            <Route path="/user/:userId" component={UserComponent} />
            <Route
              path="/foo"
              render={() => (
                <div>
                  foo
                  <Link to="/">go to main</Link>
                </div>
              )}
            />

            <DevTools />
          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default Main;
