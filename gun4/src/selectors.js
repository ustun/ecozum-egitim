function getNTodos(db) {
  return db.todos.length;
}

function getNCompletedTodos(db) {
  return db.todos.filter(todo => todo.completed).length;
}

function getNIncompleteTodos(db) {
  return db.todos.filter(todo => !todo.completed).length;
}

class FooError extends Error {}
class BarError extends Error {}
function getVisibleTodos(db) {
  switch (db.aktifFiltre) {
    case "All":
      return db.todos;
    case "Aktif":
      return db.todos.filter(todo => !todo.completed);
    case "Tamamlanmis":
      return db.todos.filter(todo => todo.completed);
    default:
      throw new FooError("gecersiz filtre");
  }
}

export {
  getNCompletedTodos,
  getVisibleTodos,
  getNIncompleteTodos,
  getNTodos,
  FooError,
  BarError
};
// import * as selectors

// const selectors =  { getNCompletedTodos, getNIncompleteTodos, getNTodos };
// export default selectors
// import selectors from './selectors'

// var isim = 'ali', soyad = "ozturk"
// ali = {isim: isim, soyad: soyad}
// ali = {isim, soyad} // Es2015

// const {name, onChange} = this.props;

// var Insan = function({ name, surname }) {
//   // const name = props.name
//   // const {name, surname} = props
//   return (
//     <div>
//       {name} {surname}
//     </div>
//   );
// };

// const getNCom  = selectors.getNcomple
// const {getNComplete} = selectors // Es2015 ayna goruntusu
// selectors.getNCompletedTodos
