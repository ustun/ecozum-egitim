import { createStore, applyMiddleware, compose } from "redux";
import uuid from "uuid/v4";
import lodash from "lodash";

const actionTypes = {
  SET_GECICI_TODO: "SET_GECICI_TODO",
  TODO_EKLE: "TODO_EKLE",
  AKTIF_FILTRE_DEGISTIR: "AKTIF_FILTRE_DEGISTIR",
  TODO_CEVIR: "TODO_CEVIR",
  TODO_SIL: "TODO_SIL"
};

const defaultTodos = [];

function TodosReducer(todos = defaultTodos, action) {
  switch (action.type) {
    case actionTypes.TODO_EKLE:
      return [
        ...todos,
        { description: action.payload, id: uuid(), completed: false }
      ];
    case actionTypes.TODO_SIL:
      return todos.filter(todo => todo.id != action.payload);
    case actionTypes.TODO_CEVIR:
      return todos.map(todo => {
        if (todo.id == action.payload) {
          return { ...todo, completed: !todo.completed };
        } else {
          return todo;
        }
      });
  }

  return todos;
}

const defaultState = {
  todos: TodosReducer(undefined, { type: "@@REDUX_INIT" }), // new TodosArray()
  geciciTodo: "",
  aktifFiltre: "All"
};

// combineReducer

// https://redux.js.org/docs/advanced/ExampleRedditAPI.html
function TodoAppReducer(state = defaultState, action) {
  switch (action.type) {
    case actionTypes.SET_GECICI_TODO:
      return { ...state, geciciTodo: action.payload };
    case actionTypes.TODO_SIL:
      return { ...state, todos: TodosReducer(state.todos, action) };
    case actionTypes.TODO_EKLE:
      return {
        ...state,
        geciciTodo: "",
        todos: TodosReducer(state.todos, {
          type: action.type,
          payload: state.geciciTodo
        })
      };
    case actionTypes.TODO_CEVIR:
      return { ...state, todos: TodosReducer(state.todos, action) };
    case actionTypes.AKTIF_FILTRE_DEGISTIR:
      return {
        ...state,
        aktifFiltre: action.payload
      };

    default:
      console.warn("Taninmayan aksiyon", action);
      return state;
  }
}

// store.dispatch({
//   type: actionTypes.SET_GECICI_TODO,
//   payload: "Merhaba Dunya"
// });

// const abone1 = function() {
//   console.log("%c Store degisti", "color: blue; font-size: 14px;");
//   console.log("Storeun son hali", store.getState());
// };

// const abone2 = function() {
//   console.log("%c Store degisti", "color: green; font-size: 14px;");
//   console.log("Storeun son hali", store.getState());
// };

// store.subscribe(abone1);
// var unsubscribe2 = store.subscribe(abone2);

// console.log(store.getState());

// store.dispatch({
//   type: actionTypes.SET_GECICI_TODO,
//   payload: "Merhaba Istanbul"
// });

// unsubscribe2();

// store.dispatch({
//   type: actionTypes.SET_GECICI_TODO,
//   payload: "Merhaba Trabzon"
// });

// const store = createMyStore();

export { actionTypes, TodosReducer, TodoAppReducer };
