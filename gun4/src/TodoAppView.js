import React from "react";
import * as selectors from "./selectors";
import { connect } from "react-redux";
import "./index.css";
import "./base.css";
import "./App.css";
import { actionTypes } from "./model";

function TodoItem({ item, actions }) {
  return (
    <li>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={item.completed}
          onChange={() => actions.onToggle(item.id)}
        />
        <label>{item.description}</label>
        <button className="destroy" onClick={() => actions.onDelete(item.id)} />
      </div>
      <input className="edit" defaultValue={item.description} />
    </li>
  );
}

const mapDispatchToPropsTodo = function(dispatch) {
  return {
    actions: {
      onToggle(id) {
        dispatch({ type: actionTypes.TODO_CEVIR, payload: id });
      },
      onDelete(id) {
        dispatch({ type: actionTypes.TODO_SIL, payload: id });
      }
    }
  };
};

const AkilliTodoItem = connect(null, mapDispatchToPropsTodo)(TodoItem);

export class TodoApp extends React.Component {
  render() {
    return (
      <section id="todoapp">
        <header id="header">
          <h1>todos</h1>
          <input
            id="new-todo"
            placeholder="What needs to be done?"
            onKeyPress={e => {
              if (e.key == "Enter") {
                this.props.insertGeciciTodo();
              }
            }}
            onChange={e => this.props.setGeciciTodo(e.target.value)}
            //   model.dispatch({
            //     type: "SET_GECICI_TODO",
            //     payload: e.target.value
            //   })}
            value={this.props.geciciTodo}
            autoFocus
          />
          <div>{this.props.geciciTodo}</div>
        </header>
        <section id="main" style={{ display: "block" }}>
          <input id="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>
          <ul id="todo-list">
            {selectors
              .getVisibleTodos(this.props)
              .map(item => <AkilliTodoItem key={item.id} item={item} />)}
          </ul>
        </section>
        <footer id="footer" style={{ display: "block" }}>
          <span id="todo-count">
            <strong>{selectors.getNIncompleteTodos(this.props)}</strong> items
            left
          </span>
          <ul id="filters">
            <li>
              <a
                className={this.props.aktifFiltre == "All" ? "selected" : ""}
                onClick={e => null}
                href="#/"
              >
                All
              </a>
            </li>
            <li>
              <a
                className={this.props.aktifFiltre == "Aktif" ? "selected" : ""}
                onClick={e => null}
                href="#/active"
              >
                Active
              </a>
            </li>
            <li>
              <a
                className={
                  this.props.aktifFiltre == "Tamamlanmis" ? "selected" : ""
                }
                onClick={e => null}
                href="#/completed"
              >
                Completed
              </a>
            </li>
          </ul>
        </footer>
      </section>
    );
  }
}

const mapDbToProps = function(db) {
  return db;
};

const mapDispatchToProps = function(dispatch) {
  return {
    setGeciciTodo(payload) {
      return dispatch({ type: actionTypes.SET_GECICI_TODO, payload });
    },
    insertGeciciTodo() {
      return dispatch({ type: actionTypes.TODO_EKLE });
    }
  };
};
const AkilliTodoApp = connect(mapDbToProps, mapDispatchToProps)(TodoApp);

export default AkilliTodoApp;
