// @flow-check
import * as React from "react";
import "./App.css";
import * as uuid from "uuid/v4";
// const logo = require("./logo.svg");

export function toplama(x: number, y: number) {
  return x + y;
}

var hobbies: Array<number> = [2, 1];

console.log(hobbies);

// hobbies.forEach(hobi => hobi.toUpperCase());

class Student {
  constructor(
    public id: number,
    public name: string,
    public dogumYili: number
  ) {}

  getAge() {
    return 2017 - this.dogumYili;
  }
}

class Todo {
  constructor(
    public description: string,
    public completed: boolean,
    public id: string
  ) {}

  toggle() {
    this.completed = !this.completed;
    return this;
  }

  static todoWithDescription(description: string) {
    return new Todo(description, false, uuid());
  }
}

enum FilterType {
  ALL = "all",
  ACTIVE = "active",
  COMPLETED = "complete"
}

// class Okuz {}
// class Inek {}

// type Animal = Okuz | Inek | number | string;

// var x: Array<Animal> = [new Okuz(), new Inek(), 3, "ali"];

// console.log(x);

class TodoAppModel {
  todos: Array<Todo> = [
    Todo.todoWithDescription("a"),
    Todo.todoWithDescription("b")
  ];
  aktifFilter: FilterType = FilterType.ALL;
  geciciTodo: string = "";

  setGeciciTodo(x: string) {
    this.geciciTodo = x;
  }

  insertGeciciTodo() {
    // var x = [1, 2, 3];

    this.todos = [
      ...this.todos,
      // ...x,
      Todo.todoWithDescription(this.geciciTodo)
    ];

    this.geciciTodo = "";
  }

  destroyTodo(id: string) {
    this.todos = this.todos.filter(todo => todo.id != id);
  }

  toggleTodo(id: string) {
    this.todos = this.todos.map(todo => {
      if (todo.id == id) {
        return todo.toggle();
      } else {
        return todo;
      }
    });
  }
}

var t = new TodoAppModel();

t.aktifFilter = FilterType.ACTIVE;

let s = new Student(1, "ustun", 1984);

s.getAge();

interface TodoComponentProps {
  item: Todo;
  onToggle(x: string): void;
  onDestroy(x: string): void;
}

function TodoComponent(props: TodoComponentProps) {
  return (
    <div>
      <button onClick={() => props.onDestroy(props.item.id)}>Destroy Me</button>
      <button onClick={() => props.onToggle(props.item.id)}>Toggle Me</button>
      Description: {props.item ? props.item.description : "description yok"}
      {props.item.completed ? "Complete" : "Incomplete"}
    </div>
  );
}

interface TodoAppProps {
  model: TodoAppModel;
  onToggle(id: string): void;
  onDestroy(id: string): void;
}

class TodoApp extends React.Component<TodoAppProps> {
  render() {
    return (
      <div>
        {this.props.model.todos.map(todo => (
          <TodoComponent
            key={todo.id}
            item={todo}
            onToggle={this.props.onToggle}
            onDestroy={this.props.onDestroy}
          />
        ))}
      </div>
    );
  }
}

interface AppStateType {
  todoApp: TodoAppModel;
}

class App extends React.Component<any, AppStateType> {
  constructor(props: any) {
    super(props);
    this.state = { todoApp: new TodoAppModel() };
  }

  onToggle(id: string) {
    this.state.todoApp.toggleTodo(id);

    this.setState({ todoApp: this.state.todoApp });
  }

  onDestroy(id: string) {
    this.state.todoApp.destroyTodo(id);

    this.setState({ todoApp: this.state.todoApp });
  }

  render() {
    return (
      <div className="App">
        <TodoApp
          model={this.state.todoApp}
          onToggle={this.onToggle.bind(this)}
          onDestroy={this.onDestroy.bind(this)}
        />
      </div>
    );
  }
}

export default App;
