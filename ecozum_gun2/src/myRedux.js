// class NormalStore {
//   name = "";
//   surname = "";

//   setName(name) {
//     this.name = name;
//   }
// }

import uuid from "uuid/v4";

class Store {
  _state = {};
  listeners = [];
  middlewares = [];

  getState() {
    return this._state;
  }

  addMiddleware(callbackFn) {
    this.middlewares.push(callbackFn);
  }

  subscribe(callbackFn) {
    this.listeners.push(callbackFn);
  }

  abonelereHaberVer() {
    this.listeners.forEach(listener => listener());
  }

  // action: {type: String, ....}
  dispatch(action) {
    //  var oldState = this.state;
    this.middlewares.forEach(middlewareFn => middlewareFn(action));

    var newState = this.reduce(this._state, action);

    this._state = newState;

    this.abonelereHaberVer();
  }

  reduce(state, action) {
    throw new Error("this is an abstract class");
  }
}

class InsanStore extends Store {
  reduce(state, action) {
    //
    let newState;

    if (action.type == "SET_NAME") {
      newState = {
        surname: state.surname,
        name: action.name
      };
    }

    if (action.type == "SET_SURNAME") {
      newState = { ...state, surname: action.surname };
    }
    return newState;
  }
}

var s = new InsanStore();

var abone1 = function() {
  console.log("Abone 1'e haber verildi");
  console.log(s.getState());
};

//s.subscribe(abone1);

s.dispatch({ type: "SET_NAME", name: "Ali" });

s.dispatch({ type: "SET_SURNAME", surname: "Ozturk" });

class TodoStore extends Store {
  _state = { todoList: [], geciciTodo: "", filter: "All" };

  reduce(state, action) {
    switch (action.type) {
      case "SET_GECICI_TODO":
        return { ...state, geciciTodo: action.text };
      case "INSERT_GECICI_TODO":
        return {
          ...state,
          geciciTodo: "",
          todoList: [
            ...state.todoList,
            {
              description: state.geciciTodo,
              id: uuid(),
              completed: false
            }
          ]
        };
      case "SET_FILTER":
        return {
          ...state,
          filter: action.filter
        };
      case "TOGGLE_TODO":
        return {
          ...state,
          todoList: state.todoList.map(todo => {
            if (todo.id === action.id) {
              return { ...todo, completed: !todo.completed };
            } else {
              return todo;
            }
          })
        };
    }
  }
}

var todoStore = new TodoStore();


