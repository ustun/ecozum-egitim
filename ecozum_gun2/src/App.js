import React, { Component } from "react";
import logo from "./logo.svg";
import uuid from "uuid/v4"; // yarn add uuid
import "./base.css";
import "./index.css";
import { setTimeout, clearInterval } from "timers";
import $ from "jquery";
// `import 'jquery-ui/Accordion'
import "./myRedux";
function sleep(timeoutSuresi) {
  return new Promise(function(successFn, failCallback) {
    setTimeout(function() {
      successFn();
    }, timeoutSuresi);
  });
}

class TodoItem extends React.Component {
  render() {
    return (
      <li>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick={this.props.onToggle}
          />
          <label>
            {this.props.description} {this.props.uuid}
          </label>
          <button className="destroy" />
        </div>
        <input className="edit" defaultValue={this.props.description} />
      </li>
    );
  }
}

const initialState = {
  inProgress: true,
  error: false,
  errorMessage: "",
  dataLoaded: false,
  todoItems: []
};
class TodoApp extends React.Component {
  state = {
    ...{
      geciciTodo: "",
      aktifFiltre: "Hepsi",
      aktifYazi: ""
    },
    ...initialState
  };

  async getResultsFilms() {
    var response = await fetch("https://swapi.co/api/films/");
    var data = await response.json();
    return data;
  }

  async getResultsPeople() {
    var response = await fetch("https://swapi.co/api/people/");
    var data = await response.json();
    return data;
  }

  async getResultsRemote() {
    var allResultsArray = await Promise.all([
      this.getResultsFilms(),
      this.getResultsPeople()
    ]);

    var allResults = [
      ...allResultsArray[0].results.map(film => ({
        title: film.title,
        episode_id: film.episode_id
      })),
      ...allResultsArray[1].results.map(person => ({
        title: person.name,
        episode_id: person.name
      }))
    ];

    return allResults;
  }

  async getResultsFake() {
    await sleep(300);

    if (Math.random() < 0.5) {
      return Promise.reject("connection timeout");
    }
    return Promise.resolve([
      { title: "ali", episode_id: 1 },
      { title: "veli", episode_id: 2 }
    ]);
  }
  componentWillMount() {
    console.log("component is mounting");
  }

  componentDidMount() {
    console.log("component is mounted");

    // this.$el = $(this.gercekDomElemani);
    // this.$el.addClass("foo");
    this.refreshData();

    // this.gercekDomElemani
    this.registerJqueryEvent();
  }

  registerJqueryEvent() {
    // if (this.intervalId) {
    //   clearInterval(this.intervalId);
    // }

    if (this.gercekDomElemani) {
      this.intervalId = setInterval(() => {
        console.log("fade toggle");
        $(this.gercekDomElemani).fadeToggle();
      }, 3000);
    }
  }

  componentDidUpdate() {
    // this.registerJqueryEvent();
  }

  componentWillUnmount() {
    console.log("component is unmounting");
    this.setState(initialState);
  }

  async refreshData() {
    try {
      // await sleep(200);
      var data = await this.getResultsRemote(); // data

      // var data = await this.getResultsFake(); // data

      this.setState({
        inProgress: false,
        dataLoaded: true,
        todoItems: data.map(film => {
          return {
            description: film.title,
            completed: false,
            id: film.episode_id
          };
        })
      });
    } catch (e) {
      this.setState({
        inProgress: false,
        error: true,
        dataLoaded: false,
        errorMessage: e.message
      });
      console.log(e);
    }
  }

  toggleTodo(toggleEdilmekIstenenTodonunIdsi) {
    var newItems = this.state.todoItems.map(function(item) {
      if (item.id == toggleEdilmekIstenenTodonunIdsi) {
        item.completed = !item.completed;
        // return { ...item, completed: !item.completed };
      }

      return item;
    });

    this.setState({ todos: newItems });
  }
  insertTodo() {
    if (this.state.geciciTodo.trim()) {
      var oldItems = this.state.todoItems;
      var newItems = [
        {
          description: this.state.geciciTodo.trim(),
          id: uuid(),
          completed: false
        },
        ...oldItems
      ];

      this.setState({ todoItems: newItems, geciciTodo: "" });
    }
  }

  handleKeyup(event) {
    if (event.key === "Enter") {
      this.insertTodo();
    }
  }

  getNItemsLeft() {
    return this.state.todoItems.filter(item => !item.completed).length;
  }

  getVisibleItems() {
    var visibleItems;

    if (this.state.aktifFiltre == "Hepsi") {
      visibleItems = this.state.todoItems;
    }
    if (this.state.aktifFiltre == "Aktif") {
      visibleItems = this.state.todoItems.filter(item => !item.completed);
    }
    if (this.state.aktifFiltre == "Tamamlanmis") {
      visibleItems = this.state.todoItems.filter(item => item.completed);
    }

    return visibleItems.filter(item =>
      item.description
        .toLowerCase()
        .includes(this.state.aktifYazi.toLowerCase())
    );
  }

  renderFooter() {
    return (
      <footer id="footer" style={{ display: "block" }}>
        <span id="todo-count">
          <strong>{this.getNItemsLeft()}</strong> items left
        </span>
        <ul id="filters">
          <li>
            <a
              onClick={e => this.setState({ aktifFiltre: "Hepsi" })}
              className={this.state.aktifFiltre == "Hepsi" ? "selected" : ""}
              href="#/"
            >
              All
            </a>
          </li>
          <li>
            <a
              onClick={e => this.setState({ aktifFiltre: "Aktif" })}
              href="#/active"
              className={this.state.aktifFiltre == "Aktif" ? "selected" : ""}
            >
              Active
            </a>
          </li>
          <li>
            <a
              onClick={e => this.setState({ aktifFiltre: "Tamamlanmis" })}
              href="#/completed"
              className={
                this.state.aktifFiltre == "Tamamlanmis" ? "selected" : ""
              }
            >
              Completed
            </a>
          </li>
        </ul>
      </footer>
    );
  }

  renderHeader() {
    return (
      <header id="header">
        <h1>todos</h1>
        <button onClick={this.refreshData.bind(this)}>Veriyi Guncell</button>

        <input
          id="new-todo"
          placeholder="What needs to be done?"
          autofocus
          value={this.state.geciciTodo}
          onKeyUp={this.handleKeyup.bind(this)}
          onChange={event => this.setState({ geciciTodo: event.target.value })}
        />
        <input
          id="new-todo"
          placeholder="Arama"
          autofocus
          value={this.state.aktifYazi}
          onChange={event => this.setState({ aktifYazi: event.target.value })}
        />
      </header>
    );
  }

  renderBody() {
    if (this.state.dataLoaded) {
      return (
        <section id="main" style={{ display: "block" }}>
          <input id="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>

          <ul id="todo-list">
            {this.getVisibleItems().map(todoItem => (
              <TodoItem
                key={todoItem.id}
                id={todoItem.id}
                description={todoItem.description}
                completed={todoItem.completed}
                onToggle={() => this.toggleTodo(todoItem.id)}
              />
            ))}
          </ul>
        </section>
      );
    } else {
      if (this.state.inProgress) {
        return <div>Loading data...</div>;
      } else {
        if (this.state.error) {
          return <div>Encountered an error: {this.state.errorMessage}</div>;
        }
      }
    }
  }

  registerDomElement(gercekDomElemani) {
    debugger;
    this.gercekDomElemani = gercekDomElemani;
  }

  render() {
    var debug = !true;
    // this?
    return (
      <section
        id="todoapp"
        ref={gercekDomElemani => (this.gercekDomElemani = gercekDomElemani)}
      >
        {debug ? (
          <pre
            style={{
              height: 200,
              backgroundColor: "yellow",
              overflow: "scroll"
            }}
          >
            {JSON.stringify(this.state, null, 4)}
          </pre>
        ) : null}

        {this.renderHeader()}
        {this.renderBody()}
        {this.renderFooter()}
      </section>
    );
  }
}

class LoginForm extends React.Component {
  render() {
    return <div>Login form</div>;
  }
}

export default class App extends React.Component {
  state = { currentPage: "Todo" };
  render() {
    let activePage;
    switch (this.state.currentPage) {
      case "Todo":
        activePage = <TodoApp />;
        break;
      case "Login":
        activePage = <LoginForm />;
        break;
    }
    return (
      <div>
        <button onClick={e => this.setState({ currentPage: "Todo" })}>
          Show Todos
        </button>
        <button onClick={e => this.setState({ currentPage: "Login" })}>
          Show Login
        </button>
        {activePage}
      </div>
    );
  }
}
